import React from 'react'
import { Switch, Route, } from 'react-router-dom'
import HomePage from '../pages/HomePage/HomePage'
import URLS from '../../constants/urls.constants'

function Routes(props) {
  return (
    <>
      <Switch>
        <Route path={URLS.HOME}>
          <HomePage />
        </Route>
      </Switch>
    </>
  )
}

export default Routes
