import React from 'react'
import { Layout } from 'antd';
import Header from './Header'
import styled from '@emotion/styled'

const { Content } = Layout;

const EmailTemplates = styled.div({
  display: 'grid',
  gridTemplateColumns: 'repeat(auto-fit, minmax(250px, 1fr))',
  gridGap: 15,
})

function HomePage(props) {
  return (
    <HomePage>
      <Header />
      <Content>
        <EmailTemplates>
          templates
        </EmailTemplates>
      </Content>
    </HomePage>
  )
}

export default HomePage