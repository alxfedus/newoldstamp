import React from 'react'
import { Layout, Button } from 'antd';
import styled from '@emotion/styled'

const { Header } = Layout

const HeaderStyled = styled(Header)({
  height: 100,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
})

function HeaderRender(props)  {
  return (
    <HeaderStyled>
      <div>Signature List</div>
      <Button type="primary" ghost>Create a signature</Button>
    </HeaderStyled>
  )
}

export default HeaderRender